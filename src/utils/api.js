const API_KEY = 'd3e7482cbe595ebca49efc7a2d82df2055bf1ff23887e1c50ad60615b2f1573b';
const AGGREGATE_INDEX = '5';
const EXCHANGE = 'CCCAGG';

let BTC = false;
const tickersHandlers = new Map();
const subscriberList = new Map();
const toBTC = new Map();
const noCoins = [];

const urlWS = new URL('/v2', 'wss://streamer.cryptocompare.com');
urlWS.searchParams.set('api_key', API_KEY);
const socket = new WebSocket(urlWS);

socket.addEventListener('message', e => {
	const {
		TYPE: type,
		FROMSYMBOL: currency,
		TOSYMBOL: target,
		PRICE: newPrice,
		PARAMETER: param,
		MESSAGE: msg
	} = JSON.parse(e.data)

	if (type === '500' && msg === 'INVALID_SUB') {
		const [current, targetCoin] = [param.split('~')[2], param.split('~')[3]]

		if (targetCoin === 'BTC') {
			noCoins.push(current);
			return;
		}

		if (toBTC.has(current) && subscriberList.has('BTC')) {
			handlersHand(toBTC.get(current) * subscriberList.get('BTC'), current);
			return;
		}

		sendToWebSocket('SubAdd', current, 'BTC')

		if (!subscriberList.has('BTC')) {
			sendToWebSocket('SubAdd', 'BTC')
		}
	}

	if (type !== AGGREGATE_INDEX || !newPrice) return;

	if (currency === 'BTC' && toBTC.size) {
		toBTC.forEach((value, key) => handlersHand(value * newPrice, key))
	}

	if (target === 'BTC') {
		toBTC.set(currency, newPrice);
		return;
	}

	subscriberList.set(currency, newPrice)
	handlersHand(newPrice, currency)
})

function handlersHand(price, name) {
	const handlers = tickersHandlers.get(name) ?? [];
	handlers.forEach(fn => fn(price))
}

function sendToWebSocket(action, from, to = 'USD') {
	const stringifiedMessage = JSON.stringify({
		"action": action,
		"subs": [`${AGGREGATE_INDEX}~${EXCHANGE}~${from}~${to}`]
	})

	if (socket.readyState === WebSocket.OPEN) {
		socket.send(stringifiedMessage);
		return;
	}

	socket.addEventListener('open', () => {
		socket.send(stringifiedMessage)
	}, {once: true})
}

function subscribeToTickerOnWS(ticker) {
	if (subscriberList.has(ticker)) {
		handlersHand(subscriberList.get(ticker), ticker);
		return;
	}
	sendToWebSocket('SubAdd', ticker)
}

function unsubscribeFromTickerOnWS(ticker) {
	if (ticker === 'BTC' && toBTC.size) {
		BTC = true;
		return;
	}
	if (toBTC.has(ticker)) {
		toBTC.delete(ticker);
		if (!toBTC.size && BTC) {
			subscriberList.delete('BTC')
			sendToWebSocket('SubRemove', 'BTC');
			BTC = false;
		}
		sendToWebSocket('SubRemove', ticker, 'BTC');
		return;
	}

	sendToWebSocket('SubRemove', ticker)
}

export const getAllCoins = (callback) => {
	const url = new URL('/data/blockchain/list', 'https://min-api.cryptocompare.com');
	url.searchParams.set('api_key', API_KEY);

	fetch(url)
		.then(response => response.json())
		.then(coinsData => {
			const coinsList = Object.values(coinsData.Data).map(t => t.symbol)
			callback(coinsList)
		})
}

export const subscribeToTicker = (ticker, callback) => {
	const subscribers = tickersHandlers.get(ticker) || [];
	tickersHandlers.set(ticker, [...subscribers, callback]);
	subscribeToTickerOnWS(ticker);
	subscriberList.set(ticker, '-');
}

export const unsubscribeToTicker = (ticker) => {
	unsubscribeFromTickerOnWS(ticker);
	subscriberList.delete(ticker);
}

export const getUndefinedCoins = () => {
	return noCoins;
}
