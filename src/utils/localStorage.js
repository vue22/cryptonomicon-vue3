const LOCAL_STORAGE_NAME = 'cryptonomicon-list';

export const getLocalStorage = () => {
	return localStorage.getItem(LOCAL_STORAGE_NAME)
}

export const setLocalStorage = (value) => {
	localStorage.setItem(LOCAL_STORAGE_NAME, JSON.stringify(value));
}