# cryptonomicon


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

## Описание
### Использованный фреймворк
Vue 3

### Возможности
Добавление и отслеживание курса криптовалюты к USD. Отображение графика изменения курса криптовалюты с моменты ее выбора.

### Особенности
Загрузка и отображение данных об изменении курса происходят в реальном времени по вебсокету.
